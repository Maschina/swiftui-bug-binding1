//
//  SwiftUI_bug_binding1App.swift
//  SwiftUI-bug-binding1
//
//  Created by Robert Hahn on 02.03.22.
//

import SwiftUI

@main
struct SwiftUI_bug_binding1App: App {
    var body: some Scene {
		let model = Model()
		
        WindowGroup {
            ContentView(model: model)
        }
    }
}
