//
//  Extensions.swift
//  SwiftUI-bug-binding1
//
//  Created by Robert Hahn on 02.03.22.
//

import Foundation
import Combine

extension NumberFormatter {
	public static func doubleFormatter(digits: Int = 1) -> NumberFormatter {
		let formatter = NumberFormatter()
		formatter.maximumFractionDigits = digits
		return formatter
	}
}

extension Collection where Indices.Iterator.Element == Index {
	public subscript(safe index: Index) -> Iterator.Element? {
		return indices.contains(index) ? self[index] : nil
	}
}

public extension Publisher {
	/// Attaches a concurrent Task-driven subscriber with closure-based behavior.
	/// - parameter receiveComplete: The closure to execute on completion.
	/// - parameter receiveValue: The closure to be awaited to execute within a concurrent Task on receipt of a value.
	/// - Returns: A cancellable instance, which you use when you end assignment of the received value. Deallocation of the result will tear down the subscription stream.
	func sink<T>(receiveCompletion: @escaping ((Subscribers.Completion<Self.Failure>) -> Void), receiveValue: @escaping ((Self.Output) async -> T)) -> AnyCancellable {
		sink { completion in
			receiveCompletion(completion)
		} receiveValue: { value in
			Task {
				await receiveValue(value)
			}
		}
		
	}
	
	/// Attaches a concurrent Task-driven subscriber with closure-based behavior.
	/// - parameter receiveComplete: The closure to be awaited to execute within a concurrent Task on completion.
	/// - parameter receiveValue: The closure to be awaited to execute within a concurrent Task on receipt of a value.
	/// - Returns: A cancellable instance, which you use when you end assignment of the received value. Deallocation of the result will tear down the subscription stream.
	func sink<T>(receiveCompletion: @escaping ((Subscribers.Completion<Self.Failure>) async -> Void), receiveValue: @escaping ((Self.Output) async -> T)) -> AnyCancellable {
		sink { completion in
			Task {
				await receiveCompletion(completion)
			}
		} receiveValue: { value in
			Task {
				await receiveValue(value)
			}
		}
		
	}
	
	/// Performs the specified closures when publisher events occur.
	/// - Parameters:
	///   - receiveOutput: An optional closure to be awaited to execute within a concurrent Task when the publisher receives a value from the upstream publisher. This value defaults to `nil`.
	func handleReceivedOutputEvent(_ receiveOutput: @escaping ((Self.Output) async -> Void)) -> Publishers.HandleEvents<Self> {
		handleEvents(receiveOutput: { output in
			Task {
				await receiveOutput(output)
			}
		})
	}
}

public extension Publisher where Self.Failure == Never {
	/// Attaches a concurrent Task-driven subscriber with closure-based behavior to a publisher that never fails.
	/// - parameter receiveValue: The closure to be awaited to execute within a concurrent Task on receipt of a value.
	/// - Returns: A cancellable instance, which you use when you end assignment of the received value. Deallocation of the result will tear down the subscription stream.
	func sink<T>(receiveValue: @escaping (Self.Output) async -> T) -> AnyCancellable {
		sink { value in
			Task {
				await receiveValue(value)
			}
		}
	}
}
