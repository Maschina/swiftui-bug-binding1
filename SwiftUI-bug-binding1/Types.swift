//
//  Types.swift
//  SwiftUI-bug-binding1
//
//  Created by Robert Hahn on 02.03.22.
//

import Foundation
import AppKit.NSColor

enum ColorValue: Equatable {
	/// Color represented in CIE xy color space
	case xy(xy: HueLight.CIEXY)
	/// Color represented with Hue and Saturation
	case huesat(hue: HueLight.Hue, saturation: HueLight.Saturation)
	/// Color represented through temperature value
	case ct(colortemp: HueLight.Colortemp)
	/// Unknown color value
	case none
	
	func getHSB(gamut: HueLight.Item.Capabilities.Control.Gamut) -> HueLight.HSB? {
		if case .huesat(hue: let hue, saturation: let saturation) = self {
			return HueLight.HSB(hue: hue, saturation: saturation, brightness: 1.0)
		} else if case .xy(xy: let xy) = self {
			return xy.getHSB(gamut: gamut)
		}
		return nil
	}
	
	func getHue(gamut: HueLight.Item.Capabilities.Control.Gamut) -> HueLight.Hue? {
		if case .huesat(hue: let hue, _) = self {
			return hue
		} else if case .xy(xy: let xy) = self {
			return xy.getHSB(gamut: gamut).hue
		}
		return nil
	}
	
	func getSaturation(gamut: HueLight.Item.Capabilities.Control.Gamut) -> HueLight.Saturation? {
		if case .huesat(_, saturation: let saturation) = self {
			return saturation
		} else if case .xy(xy: let xy) = self {
			return xy.getHSB(gamut: gamut).saturation
		}
		return nil
	}
	
	func getColortemp() -> HueLight.Colortemp? {
		guard case .ct(colortemp: let colortemp) = self else { return nil }
		return colortemp
	}
}

struct HueLight {
	typealias DataElements = [String: HueLight.Item]
	
	// MARK: Brightness Literal
	
	struct APIBrightness: ExpressibleByIntegerLiteral, Codable {
		typealias IntegerLiteralType = UInt8
		@Clamp(1...254) private var wrappedValue: IntegerLiteralType = 1
		init(integerLiteral value: IntegerLiteralType) { self.wrappedValue = value }
		init(_ value: Int) { self.wrappedValue = IntegerLiteralType(value) }
		init(_ value: Double) { self.wrappedValue = IntegerLiteralType(value) }
		
		var uint8Value: IntegerLiteralType { wrappedValue }
		var intValue: Int { Int(wrappedValue) }
		
		var normalized: Brightness {
			Brightness(Double(wrappedValue) / 254.0)
		}
		
		func encode(to encoder: Encoder) throws {
			var container = encoder.singleValueContainer()
			try container.encode(wrappedValue)
		}
		
		init(from decoder: Decoder) throws {
			let value = try decoder.singleValueContainer()
			wrappedValue = try value.decode(UInt8.self)
		}
	}
	
	struct Brightness: ExpressibleByFloatLiteral, Equatable {
		typealias FloatLiteralType = Double
		@Clamp(0.0...1.0) private var wrappedValue: FloatLiteralType = 0.0
		init(floatLiteral value: FloatLiteralType) { self.wrappedValue = value }
		init(_ value: Double) { self.wrappedValue = value }
		
		var doubleValue: FloatLiteralType { wrappedValue }
		var cgfloatValue: CGFloat { CGFloat(wrappedValue) }
		
		var scaled: APIBrightness {
			APIBrightness(wrappedValue * 254.0)
		}
		
		func scale(by factor: Double) -> Brightness {
			Brightness(self.wrappedValue * factor)
		}
		
		static func == (lhs: Brightness, rhs: Brightness) -> Bool {
			lhs.wrappedValue == rhs.wrappedValue
		}
	}
	
	// MARK: Colortemp Literal
	
	struct APIColortemp: ExpressibleByIntegerLiteral, Codable {
		typealias IntegerLiteralType = UInt16
		@Clamp(153...500) private var wrappedValue: IntegerLiteralType = 153
		init(integerLiteral value: IntegerLiteralType) { self.wrappedValue = value }
		init(_ value: Int) { self.wrappedValue = IntegerLiteralType(value) }
		init(_ value: Double) { self.wrappedValue = IntegerLiteralType(value) }
		
		var uint16Value: IntegerLiteralType { wrappedValue }
		var intValue: Int { Int(wrappedValue) }
		
		var normalized: Colortemp {
			Colortemp((Double(wrappedValue) - 153.0) / 347.0)
		}
		
		func encode(to encoder: Encoder) throws {
			var container = encoder.singleValueContainer()
			try container.encode(wrappedValue)
		}
		
		init(from decoder: Decoder) throws {
			let value = try decoder.singleValueContainer()
			wrappedValue = try value.decode(UInt16.self)
		}
	}
	
	struct Colortemp: ExpressibleByFloatLiteral, Equatable {
		typealias FloatLiteralType = Double
		@Clamp(0.0...1.0) private var wrappedValue: FloatLiteralType = 0.0
		init(floatLiteral value: FloatLiteralType) { self.wrappedValue = value }
		init(_ value: Double) { self.wrappedValue = value }
		
		var doubleValue: FloatLiteralType { wrappedValue }
		var cgfloatValue: CGFloat { CGFloat(wrappedValue) }
		
		var scaled: APIColortemp {
			APIColortemp((wrappedValue * 347.0) + 153.0)
		}
		
		static func == (lhs: Colortemp, rhs: Colortemp) -> Bool {
			lhs.wrappedValue == rhs.wrappedValue
		}
	}
	
	// MARK: Hue Literal
	
	struct APIHue: ExpressibleByIntegerLiteral, Codable {
		typealias IntegerLiteralType = UInt16
		@Clamp(0...65_535) private var wrappedValue: IntegerLiteralType = 0
		init(integerLiteral value: IntegerLiteralType) { self.wrappedValue = value }
		init(_ value: Int) { self.wrappedValue = IntegerLiteralType(value) }
		init(_ value: Double) { self.wrappedValue = IntegerLiteralType(value) }
		
		var uint16Value: IntegerLiteralType { wrappedValue }
		var intValue: Int { Int(wrappedValue) }
		
		var normalized: Hue {
			Hue(Double(wrappedValue) / 65535.0)
		}
		
		func encode(to encoder: Encoder) throws {
			var container = encoder.singleValueContainer()
			try container.encode(wrappedValue)
		}
		
		init(from decoder: Decoder) throws {
			let value = try decoder.singleValueContainer()
			wrappedValue = try value.decode(UInt16.self)
		}
	}
	
	struct Hue: ExpressibleByFloatLiteral, Equatable {
		typealias FloatLiteralType = Double
		@Clamp(0.0...1.0) private var wrappedValue: FloatLiteralType = 0.0
		init(floatLiteral value: FloatLiteralType) { self.wrappedValue = value }
		init(_ value: Double) { self.wrappedValue = value }
		
		var doubleValue: FloatLiteralType { wrappedValue }
		var cgfloatValue: CGFloat { CGFloat(wrappedValue) }
		
		var scaled: APIHue {
			APIHue(wrappedValue * 65535.0)
		}
		
		static func == (lhs: Hue, rhs: Hue) -> Bool {
			lhs.wrappedValue == rhs.wrappedValue
		}
	}
	
	// MARK: Saturation Literal
	
	struct APISaturation: ExpressibleByIntegerLiteral, Codable {
		typealias IntegerLiteralType = UInt8
		@Clamp(0...254) private var wrappedValue: IntegerLiteralType = 0
		init(integerLiteral value: IntegerLiteralType) { self.wrappedValue = value }
		init(_ value: Int) { self.wrappedValue = IntegerLiteralType(value) }
		init(_ value: Double) { self.wrappedValue = IntegerLiteralType(value) }
		
		var uint8Value: IntegerLiteralType { wrappedValue }
		var intValue: Int { Int(wrappedValue) }
		
		var normalized: Saturation {
			Saturation(Double(wrappedValue) / 254.0)
		}
		
		func encode(to encoder: Encoder) throws {
			var container = encoder.singleValueContainer()
			try container.encode(wrappedValue)
		}
		
		init(from decoder: Decoder) throws {
			let value = try decoder.singleValueContainer()
			wrappedValue = try value.decode(UInt8.self)
		}
	}
	
	struct Saturation: ExpressibleByFloatLiteral, Equatable {
		typealias FloatLiteralType = Double
		@Clamp(0.0...1.0) private var wrappedValue: FloatLiteralType = 0.0
		init(floatLiteral value: FloatLiteralType) { self.wrappedValue = value }
		init(_ value: Double) { self.wrappedValue = value }
		
		var doubleValue: FloatLiteralType { wrappedValue }
		var cgfloatValue: CGFloat { CGFloat(wrappedValue) }
		
		var scaled: APISaturation {
			APISaturation(wrappedValue * 254.0)
		}
		
		static func == (lhs: Saturation, rhs: Saturation) -> Bool {
			lhs.wrappedValue == rhs.wrappedValue
		}
	}
	
	// MARK: CIE XY Literal
	
	struct CIEXY: ExpressibleByDictionaryLiteral, Equatable, Codable {
		typealias FloatLiteralType = Double
		@Clamp(0.0...1.0) private var wrappedXValue: FloatLiteralType = 0.0
		@Clamp(0.0...1.0) private var wrappedYValue: FloatLiteralType = 0.0
		init(dictionaryLiteral elements: (String, FloatLiteralType)...) {
			for (key, value) in elements {
				if key == "x" { self.wrappedXValue = value }
				if key == "y" { self.wrappedYValue = value }
			}
		}
		init(_ tuple: (FloatLiteralType, FloatLiteralType)) {
			self.init(dictionaryLiteral: ("x", tuple.0), ("y", tuple.1))
		}
		
		var x: FloatLiteralType {
			get { wrappedXValue }
			set { wrappedXValue = newValue }
		}
		var y: FloatLiteralType {
			get { wrappedYValue }
			set { wrappedYValue = newValue }
		}
		
		func encode(to encoder: Encoder) throws {
			var container = encoder.singleValueContainer()
			let xNumber = Decimal(limitPrecision(wrappedXValue))
			let yNumber = Decimal(limitPrecision(wrappedYValue))
			try container.encode([xNumber, yNumber])
		}
		
		init(from decoder: Decoder) throws {
			let values = try decoder.singleValueContainer()
			let array = try values.decode([Double].self)
			if array.count == 2 {
				wrappedXValue = array[safe: 0] ?? 0.0
				wrappedYValue = array[safe: 1] ?? 0.0
			}
		}
		
		private func limitPrecision(_ value: FloatLiteralType, digits: Int = 4) -> FloatLiteralType {
			let factor = pow(10.0, Double(digits))
			return (factor * value).rounded() / factor
		}
		
		static func == (lhs: CIEXY, rhs: CIEXY) -> Bool {
			lhs.x == rhs.x && lhs.y == rhs.y
		}
	}
	
	
	// MARK: RGB Type
	
	struct RGB {
		@Clamp(0.0...1.0) var red: Double = 0.0
		@Clamp(0.0...1.0) var green: Double = 0.0
		@Clamp(0.0...1.0) var blue: Double = 0.0
	}
	
	
	// MARK: HSB Type
	
	struct HSB: Equatable {
		var hue: HueLight.Hue
		var saturation: HueLight.Saturation
		var brightness: HueLight.Brightness
	}
	
	
	// MARK: - Properties
	
	/// Representing the Hue Light item
	var item: Item
	
	init(id: String, item: Item) {
		self.item = item
	}
	
	
	struct Item: Codable {
		/// Details the state of the light, see the state table below for more details.
		var state: State?
		/// A fixed name describing the type of light e.g. “Extended color light”.
		var type: Type?
		/// A unique, editable name given to the light.
		var name: String?
		/// The hardware model of the light.
		var modelid: String?
		/// Unique id of the device. The MAC address of the device with a unique endpoint id in the form: AA:BB:CC:DD:EE:FF:00:11-XX
		var uniqueid: String?
		/// The manufacturer name.
		var manufacturername: String?
		
		var productname: String?
		/// Lists capabilities of the light
		var capabilities: Capabilities?
		/// This parameter uniquely identifies the software version running in the hardware.
		var swversion: String?
		/// Uniquely identifying configuration of the software running in the hardware. Not available for all devices.
		var swconfigid: String?
		/// Uniquely identifying hardware models for the given manufacturer. Not available for all devices.
		var productid: String?
		/// Additional configuration of a light
		var config: Config?
		
		var swupdate: Swupdate?
		
		enum `Type`: String, Codable {
			/// On/off light (ZigBee Device ID: 0x0000), supports groups, scenes and on/off control
			case OnOff = "On/off light"
			/// Dimmable light (ZigBee Device ID: 0x0100), which supports groups, scenes, on/off and dimming.
			case Dimmable = "Dimmable light"
			/// Color temperature light (ZigBee Device ID: 0x0220), which supports groups, scenes, on/off, dimming, and setting of a color temperature.
			case Temperature = "Color temperature light"
			/// Color light (ZigBee Device ID: 0x0200), which supports groups, scenes, on/off, dimming and color control (hue/saturation, enhanced hue, color loop and XY)
			case Color = "Color light"
			/// Extended Color light (ZigBee Device ID: 0x0210), same as Color light, but which supports additional setting of color temperature
			case Extended = "Extended color light"
			/// On/off power plug (ZigBee Device ID: 0x0010)
			case Plug = "On/Off plug-in unit"
			/// Default unknown
			case Unknown
		}
		
		
		// MARK: - State
		struct State: Codable {
			/// On/Off state of the light. On=true, Off=false
			var on: Bool?
			/// The alert effect, which is a temporary change to the bulb’s state. Note that this contains the last alert sent to the light and not its current state. i.e. after the breathe cycle has finished the bridge does not reset the alert to “none“.
			var alert: Alert?
			/// Indicates if a light can be reached by the bridge.
			var reachable: Bool?
			/// Brightness of the light. This is a scale from the minimum brightness the light is capable of, 1, to the maximum capable brightness, 254.
			var bri: HueLight.APIBrightness?
			/// The Mired Color temperature of the light. 2012 connected lights are capable of 153 (6500K) to 500 (2000K).
			var ct: HueLight.APIColortemp?
			/// Hue of the light. This is a wrapping value between 0 and 65535. Note, that hue/sat values are hardware dependent which means that programming two devices with the same value does not garantuee that they will be the same color. Programming 0 and 65535 would mean that the light will resemble the color red, 21845 for green and 43690 for blue.
			var hue: HueLight.APIHue?
			/// Saturation of the light. 254 is the most saturated (colored) and 0 is the least saturated (white).
			var sat: HueLight.APISaturation?
			/// The x and y coordinates of a color in CIE color space. Both x and y are between 0 and 1. Using CIE xy, the colors can be the same on all lamps if the coordinates are within every lamps gamuts (example: “xy”:[0.409,0.5179] is the same color on all lamps). If not, the lamp will calculate it’s closest color and use that. The CIE xy color is absolute, independent from the hardware.
			var xy: HueLight.CIEXY?
			/// Indicates the color mode in which the light is working, this is the last command type it received. Values are “hs” for Hue and Saturation, “xy” for XY and “ct” for Color Temperature. This parameter is only present when the light supports at least one of the values.
			var colormode: Colormode?
			/// Describing mode of the light, e.g. streaming (not editable), or homeautomation
			var mode: Mode?
			/// The dynamic effect of the light, can either be “none” or “colorloop”.If set to colorloop, the light will cycle through all hues using the current brightness and saturation settings.
			var effect: Effect?
			
			enum Alert: String, Codable {
				/// The lamp shall perform one breathe cycle.
				case select = "select"
				/// The lamp shall perform breathe cycles for 15 seconds or until the “alert”:”none” command is received.
				case lselect = "lselect"
				/// The lamp shall stop performing all alert effects.
				case none = "none"
			}
			
			enum Colormode: String, Codable {
				/// Hue and Saturation
				case hs = "hs"
				/// XY
				case xy = "xy"
				/// Color temperature
				case ct = "ct"
				/// Default unknown
				case unknown
			}
			
			enum Mode: String, Codable {
				/// Device is in use by a streaming client and cannot be controlled by updating /state and light output is not reflected in /state
				case streaming = "streaming"
				/// Device can be controlled by updating /state, light output is reflected in /state
				case homeautomation = "homeautomation"
			}
			
			enum Effect: String, Codable {
				case colorloop = "colorloop"
				case none = "none"
			}
		}
		
		
		// MARK: - Capabilities
		/// Lists capabilities of the light
		struct Capabilities: Codable {
			/// This device is Hue certified
			var certified: Bool?
			/// Control capabilities of light
			var control: Control?
			/// Present of light supports streaming features
			var streaming: Streaming?
			
			
			// MARK: Control
			struct Control: Codable {
				var mindimlevel: Int?
				/// Maximum lumen output. Might not be reached at all color points.
				var maxlumen: Int?
				
				var ct: CT?
				
				var colorgamuttype: Gamut?
				
				var colorgamut: GamutConstraints?
				
				
				// MARK: CT
				struct CT: Codable {
					var min: Int
					
					var max: Int
				}
				
				// MARK: Gamut
				enum Gamut: String, Codable {
					/// Legacy LivingColors Bloom, Aura, Light Strips and Iris
					case A = "A"
					/// Older model hue bulb
					case B = "B"
					/// Newer model Hue lights
					case C = "C"
					/// New or not properly given gammut
					case unknown
					/// Predefined gamut constraints for hue lights
					var constraints: GamutConstraints {
						switch self {
							case .A:
								return GamutConstraints(red: GamutConstraints.Point(0.704, 0.296), green: GamutConstraints.Point(0.2151, 0.7106), blue: GamutConstraints.Point(0.138, 0.08))
							case .B:
								return GamutConstraints(red: GamutConstraints.Point(0.675, 0.322), green: GamutConstraints.Point(0.409, 0.518), blue: GamutConstraints.Point(0.167, 0.04))
							case .C:
								return GamutConstraints(red: GamutConstraints.Point(0.6915, 0.3038), green: GamutConstraints.Point(0.17, 0.7), blue: GamutConstraints.Point(0.1532, 0.0475))
							default:
								return GamutConstraints(red: GamutConstraints.Point(1.0, 0), green: GamutConstraints.Point(0.0, 1.0), blue: GamutConstraints.Point(0.0, 0.0))
						}
					}
				}
				
				// MARK: GamutConstraints
				struct GamutConstraints: Codable {
					var red: Point
					var green: Point
					var blue: Point
					
					struct Point: Codable {
						var x: Double
						var y: Double
						
						init(_ x: Double, _ y: Double) {
							self.x = x
							self.y = y
						}
						
						init(from decoder: Decoder) throws {
							let values = try decoder.singleValueContainer()
							let array = try values.decode([Double].self)
							if array.count == 2, let x = array[safe: 0], let y = array[safe: 1] {
								self.x = x
								self.y = y
							} else {
								x = 0.0
								y = 0.0
							}
						}
						
						func encode(to encoder: Encoder) throws {
							var container = encoder.singleValueContainer()
							try container.encode([x, y])
						}
					}
					
					init(red: Point, green: Point, blue: Point) {
						self.red = red
						self.green = green
						self.blue = blue
					}
					
					init(from decoder: Decoder) throws {
						let values = try decoder.singleValueContainer()
						let array = try values.decode([Point].self)
						if array.count == 3, let red = array[safe: 0], let green = array[safe: 1], let blue = array[safe: 2] {
							self.red = red
							self.green = green
							self.blue = blue
						} else {
							self.red = Point(0, 0)
							self.green = Point(0, 0)
							self.blue = Point(0, 0)
						}
					}
					
					func encode(to encoder: Encoder) throws {
						var container = encoder.singleValueContainer()
						try container.encode([red, green, blue])
					}
					
					func contains(_ p: Point) -> Bool {
						let crossProduct = { (_ p1: CGPoint, _ p2: CGPoint) -> Float in
							Float((p1.x * p2.y - p1.y * p2.x))
						}
						
						let v1 = CGPoint(x: green.x - red.x, y: green.y - red.y)
						let v2 = CGPoint(x: blue.x - red.x, y: blue.y - red.y)
						let q = CGPoint(x: p.x - red.x, y: p.y - red.y)
						let s = crossProduct(q, v2) / crossProduct(v1, v2)
						let t = crossProduct(v1, q) / crossProduct(v1, v2)
						if (s >= 0.0) && (t >= 0.0) && (s + t <= 1.0) {
							return true
						}
						return false
					}
					
					func contains(_ p: HueLight.CIEXY) -> Bool { contains(Point(p.x, p.y)) }
					
					func closestPoint(of p: HueLight.CIEXY, vector: (A: Point, B: Point)) -> HueLight.CIEXY {
						let AP = CGPoint(x: p.x - vector.A.x, y: p.y - vector.A.y)
						let AB = CGPoint(x: vector.B.x - vector.A.x, y: vector.B.y - vector.A.y)
						let ab2 = Double(AB.x * AB.x + AB.y * AB.y)
						let ap_ab = Double(AP.x * AB.x + AP.y * AB.y)
						var t = ap_ab / ab2
						if t < 0.0 { t = 0.0 }
						else if t > 1.0 { t = 1.0 }
						let newPoint = HueLight.CIEXY((vector.A.x + AB.x * t, y: vector.A.y + AB.y * t))
						return newPoint
					}
				}
			}
			
			
			// MARK: Streaming
			/// Current light supports streaming features
			struct Streaming: Codable {
				/// Indicates if a lamp can be used for entertainment streaming as renderer
				var renderer: Bool?
				/// Indicates if a lamp can be used for entertainment streaming as a proxy node
				var proxy: Bool?
			}
		}
		
		
		// MARK: - Config
		struct Config: Codable {
			/// The shape of the light (bulb, luminaire)
			var archetype: Archetype?
			/// The major purpose of the light.
			var function: Function?
			/// The major direction of the light.
			var direction: Direction?
			/// Object describing the startup behavior of a light.
			var startup: Startup?
			
			enum Archetype: String, Codable {
				case bollard = "bollard"
				case christmastree = "christmastree"
				case candlebulb = "candlebulb"
				case ceilinground = "ceilinground"
				case ceilingsquare = "ceilingsquare"
				case classicbulb = "classicbulb"
				case doublespot = "doublespot"
				case flexiblelamp = "flexiblelamp"
				case floodbulb = "floodbulb"
				case floorlantern = "floorlantern"
				case floorshade = "floorshade"
				case generalbulb = "generalbulb"
				case groundspot = "groundspot"
				case huebloom = "huebloom"
				case huego = "huego"
				case hueiris = "hueiris"
				case huelightstrip = "huelightstrip"
				case hueplay = "hueplay"
				case pendantlong = "pendantlong"
				case pendantround = "pendantround"
				case plug = "plug"
				case recessedceiling = "recessedceiling"
				case recessedfloor = "recessedfloor"
				case singlespot = "singlespot"
				case spotbulb = "spotbulb"
				case sultanbulb = "sultanbulb"
				case tableshade = "tableshade"
				case tablewash = "tablewash"
				case vintagebulb = "vintagebulb"
				case walllantern = "walllantern"
				case wallshade = "wallshade"
				case wallspot = "wallspot"
				case unknown
			}
			
			enum Function: String, Codable {
				/// The major purpose is functional lighting. Examples include ceiling luminaires, spots, white bulbs.
				case functional = "functional"
				/// The major purpose is decorative lighting with visible impact on space ambiance. Examples include shades, candle, color bulbs.
				case decorative = "decorative"
				/// There is no clear major purpose. The use changes depending on the use case and can be functional or decorative. Examples include lantern, desk lamp, lightstrip.
				case mixed = "mixed"
				/// Other function (none of the above).
				case unknownfunction
			}
			
			enum Direction: String, Codable {
				/// Light is emitted in all directions (omni-directional). Examples include Hue Go, lightstrip, spot, lantern, bollard.
				case omnidirectional = "omnidirectional"
				/// Light emits upwards only. Examples include groundspot, recessfloor, Iris, Bloom.
				case upwards = "upwards"
				/// Light emits downwards only. Examples include ceiling lamp, wallspot.
				case downwards = "downwards"
				/// Light emits to the sides in at least 2 directions or to all directions.
				case horizontal = "horizontal"
				/// Light emits up and down. Examples include wall lanterns, pendants that also have light on top.
				case vertical = "vertical"
				/// Other direction (none of the above).
				case unknowndirection
			}
			
			
			// MARK: Startup
			/// Part of config. Configures the startup behavior
			struct Startup: Codable {
				/// Mode of the startup behavior
				var mode: String?
				/// true if the startup settings are committed to the device, false if not.  If this attribute is not present (<1.28) the bridge does not ensure the settings are committed.
				var configured: Bool?
			}
		}
		
		
		// MARK: - Swupdate
		struct Swupdate: Codable {
			/// State of software update for this device.
			var state: State?
			/// Time of last software installation.
			var lastinstall: String?
			
			enum State: String, Codable {
				/// System cannot update this device or determine if it is out-of-date.
				case notupdatable = "notupdatable"
				/// No update available nor known.
				case noupdates = "noupdates"
				/// Bridge knows there is an update is available. But not yet downloaded from portal or finished transferring to device.
				case transferring = "transferring"
				/// Software is ready to install (ie transferred to device).
				case readytoinstall = "readytoinstall"
				/// Software update is installing. Note that the device might not be usable for 30-60s during installation.
				case installing = "installing"
				/// Battery is too low for update.
				case batterylow = "batterylow"
				/// Device rejected installing image.
				case imagerejected = "imagerejected"
				/// There is an issue installing the software.
				case error = "error"
				/// Default unknown
				case unknown
			}
		}
	}
}


// MARK: - Color conversions CIEXY

extension HueLight.CIEXY {
	func getRGB(gamut: HueLight.Item.Capabilities.Control.Gamut, brightness: HueLight.Brightness = 1.0) -> HueLight.RGB {
		var xy = self
		
		// Step 1: Check if the xy value is within the color gamut of the lamp, if not continue with step 2, otherwise step 3 We do this to calculate the most accurate color the given light can actually do.
		let isWithinGamut = gamut.constraints.contains(xy)
		
		// Step 2: Calculate the closest point on the color gamut triangle and use that as xy value
		if !isWithinGamut { // It seems the colour is out of reach, so let's find the closest producable color with the lamp
			// Find the closest point on each line in the triangle
			let pAB = gamut.constraints.closestPoint(of: xy, vector: (A: gamut.constraints.red, B: gamut.constraints.green))
			let pAC = gamut.constraints.closestPoint(of: xy, vector: (A: gamut.constraints.blue, B: gamut.constraints.red))
			let pBC = gamut.constraints.closestPoint(of: xy, vector: (A: gamut.constraints.green,B: gamut.constraints.blue))
			
			// Get the distances per point and see which point is closer to our Point.
			let dAB = xy.getDistance(to: pAB)
			let dAC = xy.getDistance(to: pAC)
			let dBC = xy.getDistance(to: pBC)
			
			var lowest = dAB
			var closestPoint = pAB
			if dAC < lowest {
				lowest = dAC
				closestPoint = pAC
			}
			if dBC < lowest {
				lowest = dBC
				closestPoint = pBC
			}
			// Change the xy value to a value which is within the reach of the lamp.
			xy.x = closestPoint.x
			xy.y = closestPoint.y
		}
		
		// Step 3: Calculate XYZ values
		let x: Double = xy.x // the given x value
		let y: Double = xy.y // the given y value
		let z: Double = 1.0 - x - y
		let Y: Double = brightness.doubleValue // The given brightness value
		let X: Double = (Y / y) * x
		let Z: Double = (Y / y) * z
		
		// Step 4: Convert to RGB using Wide RGB D65 conversion
		var r: Double = X * 1.656492 - Y * 0.354851 - Z * 0.255038
		var g: Double = -X * 0.707196 + Y * 1.655397 + Z * 0.036152
		var b: Double = X * 0.051713 - Y * 0.121364 + Z * 1.011530
		if r > b && r > g && r > 1.0 {
			// red is too big
			g = g / r
			b = b / r
			r = 1.0
		} else if g > b && g > r && g > 1.0 {
			// green is too big
			r = r / g
			b = b / g
			g = 1.0
		} else if b > r && b > g && b > 1.0 {
			// blue is too big
			r = r / b
			g = g / b
			b = 1.0
		}
		
		// Step 5: Apply reverse gamma correction
		r = (r <= 0.0031308) ? (12.92 * r) : ((1.0 + 0.055) * pow(r, (1.0 / 2.4)) - 0.055)
		g = (g <= 0.0031308) ? (12.92 * g) : ((1.0 + 0.055) * pow(g, (1.0 / 2.4)) - 0.055)
		b = (b <= 0.0031308) ? (12.92 * b) : ((1.0 + 0.055) * pow(b, (1.0 / 2.4)) - 0.055)
		if r > b && r > g {
			// red is biggest
			if r > 1.0 {
				g = g / r
				b = b / r
				r = 1.0
			}
		} else if g > b && g > r {
			// green is biggest
			if g > 1.0 {
				r = r / g
				b = b / g
				g = 1.0
			}
		} else if b > r && b > g {
			// blue is biggest
			if b > 1.0 {
				r = r / b
				g = g / b
				b = 1.0
			}
		}
		
		return HueLight.RGB(red: r, green: g, blue: b)
	}
	
	func getRGB(gamut: HueLight.Item.Capabilities.Control.Gamut, brightness: HueLight.Brightness = 1.0, withAlpha: Bool = true) -> NSColor {
		let rgb = getRGB(gamut: gamut, brightness: brightness)
		return NSColor(red: rgb.red, green: rgb.green, blue: rgb.blue, alpha: withAlpha ? brightness.cgfloatValue : 1)
	}
	
	func getHSB(gamut: HueLight.Item.Capabilities.Control.Gamut, brightness: HueLight.Brightness = 1.0) -> HueLight.HSB {
		let rgb = getRGB(gamut: gamut, brightness: brightness)
		
		let min = rgb.red < rgb.green ? (rgb.red < rgb.blue ? rgb.red : rgb.blue) : (rgb.green < rgb.blue ? rgb.green : rgb.blue)
		let max = rgb.red > rgb.green ? (rgb.red > rgb.blue ? rgb.red : rgb.blue) : (rgb.green > rgb.blue ? rgb.green : rgb.blue)
		
		let v = max
		let delta = max - min
		
		guard delta > 0.00001 else { return HueLight.HSB(hue: 0.0, saturation: 0.0, brightness: max.toBrightness()) }
		guard max > 0 else { return HueLight.HSB(hue: 0.0, saturation: 0.0, brightness: v.toBrightness()) } // Undefined, achromatic grey
		let s = delta / max
		
		let hue: (Double, Double) -> Double = { max, delta -> Double in
			if rgb.red == max { return (rgb.green - rgb.blue)/delta } // between yellow & magenta
			else if rgb.green == max { return 2 + (rgb.blue - rgb.red)/delta } // between cyan & yellow
			else { return 4 + (rgb.red - rgb.green)/delta } // between magenta & cyan
		}
		
		let h = hue(max, delta) * 60 // In degrees
		
		return HueLight.HSB(hue: HueLight.Hue((h < 0 ? h+360 : h) / 360.0), saturation: s.toSaturation(), brightness: v.toBrightness())
	}
	
	/// Returns the distance between two points
	func getDistance(to: HueLight.CIEXY) -> Double {
		let dx = Double(self.x - to.x) // horizontal difference
		let dy = Double(self.y - to.y) // vertical difference
		let dist = sqrt(dx * dx + dy * dy)
		return dist
	}
}


// MARK: - Color conversions RGB

extension HueLight.RGB {
	func getCIEXY(gamut: HueLight.Item.Capabilities.Control.Gamut) -> HueLight.CIEXY {
		// Step 1: Apply gamma correction
		let red: Double = (self.red > 0.04045) ? pow((self.red + 0.055) / (1.0 + 0.055), 2.4) : (self.red / 12.92)
		let green: Double = (self.green > 0.04045) ? pow((self.green + 0.055) / (1.0 + 0.055), 2.4) : (self.green / 12.92)
		let blue: Double = (self.blue > 0.04045) ? pow((self.blue + 0.055) / (1.0 + 0.055), 2.4) : (self.blue / 12.92)
		
		// Step 2: Convert the RGB values to XYZ using the Wide RGB D65 conversion
		let X = red * 0.664511 + green * 0.154324 + blue * 0.162028
		let Y = red * 0.283881 + green * 0.668433 + blue * 0.047685
		let Z = red * 0.000088 + green * 0.072310 + blue * 0.986039
		
		// Step 3: Calculate xy values
		let x = X / (X + Y + Z)
		let y = Y / (X + Y + Z)
		var xy = HueLight.CIEXY((x, y))
		
		// Step 4: Check if the found xy value is within the color gamut of the light, if not continue with step 5, otherwise step 6
		let isWithinGamut = gamut.constraints.contains(xy)
		
		// Step 5: Calculate the closest point on the color gamut triangle and use that as xy value
		if !isWithinGamut {
			// Find the closest point on each line in the triangle
			let pAB = gamut.constraints.closestPoint(of: xy, vector: (A: gamut.constraints.red, B: gamut.constraints.green))
			let pAC = gamut.constraints.closestPoint(of: xy, vector: (A: gamut.constraints.blue, B: gamut.constraints.red))
			let pBC = gamut.constraints.closestPoint(of: xy, vector: (A: gamut.constraints.green,B: gamut.constraints.blue))
			
			// Get the distances per point and see which point is closer to our Point.
			let dAB = xy.getDistance(to: pAB)
			let dAC = xy.getDistance(to: pAC)
			let dBC = xy.getDistance(to: pBC)
			
			var lowest = dAB
			var closestPoint = pAB
			if dAC < lowest {
				lowest = dAC
				closestPoint = pAC
			}
			if dBC < lowest {
				lowest = dBC
				closestPoint = pBC
			}
			// Change the xy value to a value which is within the reach of the lamp.
			xy.x = closestPoint.x
			xy.y = closestPoint.y
		}
		
		// Step 6: Return value
		return xy
	}
}


// MARK: - Color conversions HSB

extension HueLight.HSB {
	func getRGB() -> HueLight.RGB {
		if self.saturation.doubleValue == 0 { return HueLight.RGB(red: self.brightness.doubleValue, green: self.brightness.doubleValue, blue: self.brightness.doubleValue) } // Achromatic grey
		
		let sector = self.hue.doubleValue * 6
		let i = floor(sector)
		let f = sector - i // Factorial part of hue
		
		let p = self.brightness.doubleValue * (1 - self.saturation.doubleValue)
		let q = self.brightness.doubleValue * (1 - (self.saturation.doubleValue * f))
		let t = self.brightness.doubleValue * (1 - (self.saturation.doubleValue * (1 - f)))
		
		switch i {
			case 0:
				return HueLight.RGB(red: self.brightness.doubleValue, green: t, blue: p)
			case 1:
				return HueLight.RGB(red: q, green: self.brightness.doubleValue, blue: p)
			case 2:
				return HueLight.RGB(red: p, green: self.brightness.doubleValue, blue: t)
			case 3:
				return HueLight.RGB(red: p, green: q, blue: self.brightness.doubleValue)
			case 4:
				return HueLight.RGB(red: t, green: p, blue: self.brightness.doubleValue)
			default:
				return HueLight.RGB(red: self.brightness.doubleValue, green: p, blue: q)
		}
	}
}


// MARK: - Extensions for State

extension HueLight.Item.State {
	/// Tries to retrieve value from bridge or from given attributes (depending on given data)
	var calculatedColormode: Colormode {
		// return value from bridge, if given
		if let colormode = self.colormode {
			return colormode
		}
		
		// calculate value, if colormode not given
		if (self.ct == nil) { return .ct }
		else if !(self.hue == nil) { return .hs }
		else if !(self.xy == nil) { return .xy }
		
		return .unknown
	}
}

extension HueLight.Item.State {
	/// Brightness of the light. This is a scale from the minimum brightness the light is capable of, 0, to the maximum capable brightness, 1.
	var briNorm: HueLight.Brightness? {
		get {
			return self.bri?.normalized
		}
		set {
			guard let value = newValue?.scaled else { return }
			self.bri = value
		}
	}
	
	/// Saturation of the light. 1 is the most saturated (colored) and 0 is the least saturated (white).
	var satNorm: HueLight.Saturation? {
		get {
			return self.sat?.normalized
		}
		set {
			guard let value = newValue?.scaled else { return }
			self.sat = value
		}
	}
	
	/// Hue of the light. This is a wrapping value between 0 and 1. Note, that hue/sat values are hardware dependent which means that programming two devices with the same value does not garantuee that they will be the same color. Programming 0 and 1 would mean that the light will resemble the color red, 21845 for green and 43690 for blue.
	var hueNorm: HueLight.Hue? {
		get {
			return self.hue?.normalized
		}
		set {
			guard let value = newValue?.scaled else { return }
			self.hue = value
		}
	}
	
	/// The Mired Color temperature of the light. 2012 connected lights are capable of 0 (6500K) to 1 (2000K).
	var ctNorm: HueLight.Colortemp? {
		get {
			return self.ct?.normalized
		}
		set {
			guard let value = newValue?.scaled else { return }
			self.ct = value
		}
	}
	
	/// Calculated Hue-Saturation-Brightness of the light, based on the current colormode
	@available(*, deprecated)
	func getHSB() -> (hue: HueLight.Hue, saturation: HueLight.Saturation, brightness: HueLight.Brightness)? { // TODO: Depcreated! Replace/remove
		switch calculatedColormode {
				
			case .ct: // calculate from hue and saturation
				guard let hue = self.hueNorm, let saturation = self.satNorm, let brightness = self.briNorm else { return nil }
				return (hue: hue, saturation: saturation, brightness: brightness)
				
			case .xy: // calculate from CIE XYZ
				guard let xy = self.xy, let brightness = self.briNorm else { return nil }
				let rgb = getRGB(fromXY: xy, withBrightness: brightness.doubleValue)
				let r: Double = Double(rgb.redComponent)
				let g: Double = Double(rgb.greenComponent)
				let b: Double = Double(rgb.blueComponent)
				
				let min = r < g ? (r < b ? r : b) : (g < b ? g : b)
				let max = r > g ? (r > b ? r : b) : (g > b ? g : b)
				
				let v = max
				let delta = max - min
				
				guard delta > 0.00001 else { return (hue: 0.0, saturation: 0.0, brightness: max.toBrightness()) }
				guard max > 0 else { return (hue: 0.0, saturation: 0.0, brightness: v.toBrightness()) } // Undefined, achromatic grey
				let s = delta / max
				
				let hue: (Double, Double) -> Double = { max, delta -> Double in
					if r == max { return (g-b)/delta } // between yellow & magenta
					else if g == max { return 2 + (b-r)/delta } // between cyan & yellow
					else { return 4 + (r-g)/delta } // between magenta & cyan
				}
				
				let h = hue(max, delta) * 60 // In degrees
				
				return (hue: HueLight.Hue((h < 0 ? h+360 : h) / 360.0), saturation: s.toSaturation(), brightness: v.toBrightness())
				
			default: return nil
		}
	}
	
	/// Calculated color of the light, based on the currently set colormode
	@available(*, deprecated)
	func getColor(withAlpha: Bool = true) -> NSColor? { // TODO: Depcreated! Replace/remove
		switch calculatedColormode {
				
			case .ct: // Calculate from temperature
				guard let ct = self.ctNorm else { return nil }
				guard let brightness = self.briNorm else { return nil }
				let color = HueLight.Item.State.getRGB(fromTemperature: ct)
				return withAlpha ? color.withAlphaComponent(brightness.cgfloatValue) : color
				
			case .hs: // Calculate from hue and saturation
				guard let hue = self.hueNorm, let saturation = self.satNorm, let brightness = self.briNorm else { return nil }
				let color = NSColor(hue: hue.cgfloatValue, saturation: saturation.cgfloatValue, brightness: 1.0, alpha: withAlpha ? brightness.cgfloatValue : 1)
				return color
				
			case .xy: // Calculate from CIE XYZ
				guard let xy = self.xy, let brightness = self.briNorm else { return nil }
				let rgb = getRGB(fromXY: xy, withBrightness: brightness.doubleValue)
				let color = NSColor(red: rgb.redComponent, green: rgb.greenComponent, blue: rgb.blueComponent, alpha: withAlpha ? brightness.cgfloatValue : 1)
				return color
				
			default: return nil
		}
	}
	
	/// Converting xy and brightness values to RGB values.
	/// - Parameter xy: xy values ranging from 0 to 1
	/// - Parameter brightness: brightness values ranging from 0 to 1
	@available(*, deprecated, renamed: "HueLight.CIEXY.getRGB")
	func getRGB(fromXY: HueLight.CIEXY, withBrightness: Double) -> NSColor { // TODO: Depcreated! Replace/remove
		let x: Double = fromXY.x; // the given x value
		let y: Double = fromXY.y; // the given y value
		let z: Double = 1.0 - x - y
		let Y: Double = withBrightness // The given brightness value
		let X: Double = (Y / y) * x
		let Z: Double = (Y / y) * z
		
		// Convert to RGB using Wide RGB D65 conversion
		var r: Double =  X * 1.656492 - Y * 0.354851 - Z * 0.255038
		var g: Double = -1.0 * X * 0.707196 + Y * 1.655397 + Z * 0.036152
		var b: Double =  X * 0.051713 - Y * 0.121364 + Z * 1.011530
		
		if (r > b && r > g && r > 1.0) {
			// red is too big
			g = g / r
			b = b / r
			r = 1.0
		}
		else if (g > b && g > r && g > 1.0) {
			// green is too big
			r = r / g
			b = b / g
			g = 1.0
		}
		else if (b > r && b > g && b > 1.0) {
			// blue is too big
			r = r / b
			g = g / b
			b = 1.0
		}
		
		// Apply reverse gamma correction
		r = (r <= 0.0031308) ? 12.92 * r : (1.0 + 0.055) * pow(r, (1.0 / 2.4)) - 0.055
		g = (g <= 0.0031308) ? 12.92 * g : (1.0 + 0.055) * pow(g, (1.0 / 2.4)) - 0.055
		b = (b <= 0.0031308) ? 12.92 * b : (1.0 + 0.055) * pow(b, (1.0 / 2.4)) - 0.055
		
		if (r > b && r > g) {
			// red is biggest
			if (r > 1.0) {
				g = g / r
				b = b / r
				r = 1.0
			}
		}
		else if (g > b && g > r) {
			// green is biggest
			if (g > 1.0) {
				r = r / g
				b = b / g
				g = 1.0
			}
		}
		else if (b > r && b > g) {
			// blue is biggest
			if (b > 1.0) {
				r = r / b
				g = g / b
				b = 1.0
			}
		}
		
		return NSColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1.0)
	}
	
	
	/// Converting temperature value to RGB values
	/// - Parameter ct: color temperature value
	/// - Parameter offset: offset can fine tune the gradient to make it more visible for the end user
	static func getRGB(fromTemperature ct: HueLight.Colortemp, offset: Int = 2000) -> NSColor { // TODO: Depcreated! Replace/remove
		let mired = ((ct.doubleValue * 347.0) + 153.0)
		let kelvin = 1000000 / mired
		// All calculations require tmpKelvin \ 100, so only do the conversion once
		let temperature = (kelvin + Double(offset)) / 100.0
		var red: Int
		var green: Int
		var blue: Int
		
		// Calculate Red:
		if temperature <= 66 {
			red = 255
		} else {
			red = Int(329.698727446 * (pow((temperature - 60.0), -0.1332047592)))
			if red < 0 { red = 0 }
			if red > 255 { red = 255 }
		}
		
		// Calculate Green:
		if temperature <= 66 {
			green = Int(99.4708025861 * logf(Float(temperature)) - 161.1195681661)
		} else {
			green = Int(288.1221695283 * (pow((temperature - 60.0), -0.0755148492)))
		}
		if green < 0 { green = 0 }
		if green > 255 { green = 255 }
		
		// Calculate Blue:
		if temperature >= 66 {
			blue = 255
		} else {
			if temperature <= 19 {
				blue = 0
			} else {
				blue = Int(138.5177312231 * logf(Float(temperature) - 10) - 305.0447927307)
				if blue < 0 { blue = 0 }
				if blue > 255 { blue = 255 }
			}
		}
		
		return NSColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1)
	}
}


// MARK: - Type Conversions

extension Int {
	func toAPIBrightness() -> HueLight.APIBrightness { HueLight.APIBrightness(self) }
	func toAPIColortemp() -> HueLight.APIColortemp { HueLight.APIColortemp(self) }
	func toAPIHue() -> HueLight.APIHue { HueLight.APIHue(self) }
	func toAPISaturation() -> HueLight.APISaturation { HueLight.APISaturation(self) }
}

extension Double {
	func toBrightness() -> HueLight.Brightness { HueLight.Brightness(self) }
	func toColortemp() -> HueLight.Colortemp { HueLight.Colortemp(self) }
	func toHue() -> HueLight.Hue { HueLight.Hue(self) }
	func toSaturation() -> HueLight.Saturation { HueLight.Saturation(self) }
}

extension CGFloat {
	func toBrightness() -> HueLight.Brightness { HueLight.Brightness(self) }
	func toColortemp() -> HueLight.Colortemp { HueLight.Colortemp(self) }
	func toHue() -> HueLight.Hue { HueLight.Hue(self) }
	func toSaturation() -> HueLight.Saturation { HueLight.Saturation(self) }
}
