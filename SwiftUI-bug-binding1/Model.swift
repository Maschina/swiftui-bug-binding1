//
//  Model.swift
//  SwiftUI-bug-binding1
//
//  Created by Robert Hahn on 02.03.22.
//

import Foundation
import Combine

class Model: ObservableObject {
	@Published public var colorState: ColorValue = .huesat(hue: 0.0, saturation: 0.0)
	public var colorControl = PassthroughSubject<ColorValue, Never>()
	
	let gamut: HueLight.Item.Capabilities.Control.Gamut = .C
	
	private var cancellableCntrlSubscribers = Set<AnyCancellable>()
	
	init() {
		colorControl
			.handleReceivedOutputEvent { value in 
				DispatchQueue.main.async {
					self.colorState = value
				}
			} // update UI state
			.sink { [weak self] value in
				var body = HueLight.Item.State()
				guard let gamut = self?.gamut, let hsb = value.getHSB(gamut: gamut) else { return }
				body.hueNorm = hsb.hue
				body.satNorm = hsb.saturation
//				await self?.put(function: .state, body: body, debugMessage: "Changed 'color value' of light")
			}
			.store(in: &cancellableCntrlSubscribers)
	}
}
