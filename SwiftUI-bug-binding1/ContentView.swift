//
//  ContentView.swift
//  SwiftUI-bug-binding1
//
//  Created by Robert Hahn on 02.03.22.
//

import SwiftUI

struct ContentView: View {
	@ObservedObject var model: Model
	
    var body: some View {
		if let hsb = model.colorState.getHSB(gamut: model.gamut) {
			HSBColorSelector(hsb: Binding(get: { hsb }, set: { model.colorControl.send(.huesat(hue: $0.hue, saturation: $0.saturation)) }))
		} else {
			Text("No HSB information")
		}
    }
}

struct HSBColorSelector: View {
	@Binding var hsb: HueLight.HSB
	
	var hueProxy: Binding<Double> {
		Binding<Double>(
			get: { hsb.hue.doubleValue * 360.0 },
			set: { hsb.hue = ($0 / 360.0).toHue() }
		)
	}
	
	var saturationProxy: Binding<Double> {
		Binding<Double>(
			get: { hsb.saturation.doubleValue * 100.0 },
			set: { hsb.saturation = ($0 / 100.0).toSaturation() }
		)
	}
	
	var body: some View {
		VStack {
			HStack(alignment: .firstTextBaseline) {
				Text("Hue (0-360):")
					.frame(width: 110, alignment: .trailing)
				
				Slider(value: hueProxy, in: 0...360)
				
				VStack {
					TextField("", value: hueProxy, formatter: NumberFormatter.doubleFormatter(digits: 0))
						
					Text("Standard TextField")
				}
				.frame(width: 50)
				
				VStack {
					UnitNumberField(value: hueProxy, unitText: "°", range: 0...360, formatter: NumberFormatter.doubleFormatter(digits: 0), bezelStyle: .roundedBezel, controlSize: .small)
						.border(Color.red, width: 2)
					
					Text("Custom NSTextField")
				}
				.frame(width: 50)
			}
			
			HStack(alignment: .firstTextBaseline) {
				Text("Saturation (0-100):")
					.frame(width: 110, alignment: .trailing)
				
				Slider(value: saturationProxy, in: 0...100)
				
				VStack {
					TextField("", value: saturationProxy, formatter: NumberFormatter.doubleFormatter(digits: 0))
						
					Text("Standard TextField")
				}
				.frame(width: 50)
				
				VStack {
					UnitNumberField(value: saturationProxy, unitText: "%", range: 0...100, formatter: NumberFormatter.doubleFormatter(digits: 0), bezelStyle: .roundedBezel, controlSize: .small)
						.border(Color.red, width: 2)
						
					Text("Custom NSTextField")
				}
				.frame(width: 50)
			}
		}
		.controlSize(.small)
	}
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
		let model = Model()
		ContentView(model: model)
			.frame(width: 350)
    }
}
