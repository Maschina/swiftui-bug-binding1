//
//  Wrappers.swift
//  SwiftUI-bug-binding1
//
//  Created by Robert Hahn on 02.03.22.
//

import Foundation

@propertyWrapper
struct Clamp<V: Comparable> {
	var value: V
	let range: ClosedRange<V>
	let precision: Int?
	
	init(wrappedValue value: V, _ range: ClosedRange<V>, precision: Int? = nil) {
		self.range = range
		self.value = min(max(range.lowerBound, value), range.upperBound)
		self.precision = precision
	}
	
	var wrappedValue: V {
		get { value }
		set { value = min(max(range.lowerBound, newValue), range.upperBound) }
	}
}
